This is an implementation of Bluetooth three-player Pong game.

![gif](images/pong.gif?raw=true "gif")

![1](images/1.jpeg?raw=true "1")
![2](images/2.jpeg?raw=true "2")
![3](images/3.jpeg?raw=true "3")
![4](images/4.jpeg?raw=true "4")
![5](images/5.jpeg?raw=true "5")
